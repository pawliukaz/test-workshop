<?php

namespace App\Tests\EventListener;

use App\Entity\Item;
use App\Entity\TimestampableEntityInterface;
use App\EventListener\TimestampableListener;
use App\Utils\DateTimeUtil;
use Doctrine\ORM\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;

class TimestampableListenerTest extends TestCase
{
    /**
     * @return array
     */
    public function getPreUpdateData()
    {
        $out = [];
        $entity = new Item();
        $expected = clone $entity;
        $expected->setModified(new \DateTime('2019-03-21 13:45:00'));
        $out[] = [$expected, $entity];

        return $out;
    }

    /**
     * @param TimestampableEntityInterface $expected
     * @param TimestampableEntityInterface $entity
     *
     * @dataProvider getPreUpdateData
     */
    public function testPreUpdate($expected, $entity)
    {
        $listener = new TimestampableListener($this->getDateTimeUtilMock());
        $listener->preUpdate($this->getLifecycleEventArgsMock($entity));
        $this->assertEquals($expected, $entity);
    }

    /**
     * @param TimestampableEntityInterface $entity
     *
     * @return LifecycleEventArgs|\PHPUnit\Framework\MockObject\MockObject
     */
    public function getLifecycleEventArgsMock(TimestampableEntityInterface $entity)
    {
        $mock = $this->getMockBuilder(LifecycleEventArgs::class)->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())->method('getEntity')->willReturn($entity);

        return $mock;
    }

    /**
     * @return DateTimeUtil|\PHPUnit\Framework\MockObject\MockObject
     */
    public function getDateTimeUtilMock()
    {
        $mock = $this->getMockBuilder(DateTimeUtil::class)->disableOriginalConstructor()->getMock();
        $mock->expects($this->any())
            ->method('getCurrentDate')
            ->willReturn(new \DateTime('2019-03-21 13:45:00'));

        return $mock;
    }
}
