<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\HttpKernel\Client;

class ItemGroupControllerTest extends WebTestCase
{
    /** @var  Application $application */
    protected static $application;

    /** @var  Client $client */
    protected $client;


    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::runCommand('doctrine:database:drop --env=test --force');
        self::runCommand('doctrine:database:create --env=test');
        self::runCommand('doctrine:schema:create --env=test');

        $this->client = static::createClient();

        parent::setUp();
    }

    /**
     * @param $command
     *
     * @return int
     * @throws \Exception
     */
    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    /**
     * @return array
     */
    public function getIndexData()
    {
        $out = [];
        $out['Test index'] = [
            'Item Groups',
            200,
        ];

        return $out;
    }

    /**
     * @dataProvider getIndexData
     *
     * @param $expected
     * @param $expectedStatus
     */
    public function testIndex($expected, $expectedStatus)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/item-group/');
        $this->assertSame($expectedStatus, $client->getResponse()->getStatusCode());
        $this->assertContains($expected, $crawler->filter('h1')->text());
    }


    /**
     *
     */
    public function testNew()
    {
        //TODO something missing here
    }

    /**
     * @return Application
     */
    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        self::runCommand('doctrine:database:drop --env=test --force');
        parent::tearDown();
    }
}
