<?php

namespace App\Tests\Service;

use App\Entity\Item;
use App\Entity\ItemOrder;
use App\Repository\ItemRepository;
use App\Service\ItemService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class ItemServiceTest extends TestCase
{
    /**
     * @return array
     */
    public function getCountItemQuantityData()
    {
        $out = [];
        $quantity = 2;
        $itemOrder = new ItemOrder();
        $itemOrder->setQuantity($quantity);
        $item = new Item();

        $out['New Item'] = [
            $quantity,
            $itemOrder,
            $item,
        ];

        $quantity = 2;
        $itemOrder = new ItemOrder();
        $itemOrder->setQuantity($quantity);
        $item = new Item();
        $item->setQuantity(2);
        $out['Old Item'] = [
            4,
            $itemOrder,
            $item,
        ];

        return $out;
    }

    /**
     * @param int       $expect
     * @param ItemOrder $itemOrder
     * @param Item      $item
     *
     * @dataProvider getCountItemQuantityData
     */
    public function testCountItemQuantity($expect, $itemOrder, $item)
    {
        $service = new ItemService($this->getEntityManagerMock(), null);
        $this->assertEquals($expect, $service->countItemQuantity($itemOrder, $item));
    }

    /**
     *
     */
    public function testCreateItemByOrder()
    {
        //TODO something missing here
    }

    /**
     * @param bool|Item|null $item
     *
     * @return EntityManager|\PHPUnit\Framework\MockObject\MockObject
     */
    private function getEntityManagerMock()
    {
        $mock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        return $mock;
    }
}
