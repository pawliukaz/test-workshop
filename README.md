#UNITTEST WORKSHOP

## INSTALLATION

1. `$ cp .env.dist .env`
1. `$ scripts/start-dev.sh`
1. `$ scripts/backend.sh`
1. `$ composer install`
1. `$ bin/phpunit`
1. `$ sf d:m:m`

## USAGE

1. Run [http://localhost:8080](http://localhost:8080)

## LEGENDA

Sandelio valdymo sistema WareHouse.
Sistemos funkcionalumas:

1.  Sistema turi vaizduoti daiktus, likučiuos, sukūrimo ir modifikavimo datas pirmajam puslapyje (Name, Sku, Quantity, Created, Modified).
1.  Sistema turi pridėti/redaguoti prekes (Name, Sku, Quantity, Description, Group)
1.  Sistema turi pridėti/redaguoti uzsakyma (Sku, Quantity), pagal sku numeri sistema turi atpažinti ar yra tokia prekė sandėlyje.
    1. Jei nėra sistema sukuria naują daiktą, į daikto pavadinima (Name) yra irasoma sku numeris. Daikto  kiekis (quantity) nurodomas toks pat kaip užsakomas kiekis
    1. Jei daiktas yra sandelyje jau egzistuoja pagal sku numeri, turi būti pridėta perkamas kiekis (quantity) prie turimo daikto sandėlyje
1.  Sistema turi prideti/redaguoti daiktu grupės.

###Testuojamos klases

`Service/ItemService.php`

###Testai

`Service/ItemServiceTest.php`

###Funkciniai testai

`Controller/ItemGroupControllerTest.php`




