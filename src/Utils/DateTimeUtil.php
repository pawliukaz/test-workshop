<?php

namespace App\Utils;

class DateTimeUtil
{
    /**
     * @param string $time
     *
     * @return \DateTimeInterface
     */
    public function getCurrentDate(string $time = 'now'): \DateTimeInterface
    {
        return new \DateTime($time);
    }
}
