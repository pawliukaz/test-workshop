<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    /**
     * ItemRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Item::class);
    }

    /**
     * @param string $sku
     *
     * @return Item
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySku(string $sku): ?Item
    {
        $queryBuilder = $this->createQueryBuilder('i');
        $queryBuilder->where($queryBuilder->expr()->eq('i.sku', ':sku'))
            ->setParameter('sku', $sku);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $sku
     *
     * @return Item[]|array
     */
    public function findBySku(string $sku): array
    {
        $queryBuilder = $this->createQueryBuilder('i');
        $queryBuilder->where($queryBuilder->expr()->eq('i.sku', ':sku'))
            ->setParameter('sku', $sku);

        return $queryBuilder->getQuery()->getResult();
    }

}
