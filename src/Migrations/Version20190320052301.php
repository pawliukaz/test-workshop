<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190320052301 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, item_group_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, sku VARCHAR(8) NOT NULL, description LONGTEXT DEFAULT NULL, quantity INT NOT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, INDEX IDX_1F1B251E9259118C (item_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_order (id INT AUTO_INCREMENT NOT NULL, quantity INT DEFAULT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_order_item (item_order_id INT NOT NULL, item_id INT NOT NULL, INDEX IDX_EF5B3703E192A5F3 (item_order_id), INDEX IDX_EF5B3703126F525E (item_id), PRIMARY KEY(item_order_id, item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E9259118C FOREIGN KEY (item_group_id) REFERENCES item_group (id)');
        $this->addSql('ALTER TABLE item_order_item ADD CONSTRAINT FK_EF5B3703E192A5F3 FOREIGN KEY (item_order_id) REFERENCES item_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE item_order_item ADD CONSTRAINT FK_EF5B3703126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item_order_item DROP FOREIGN KEY FK_EF5B3703126F525E');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E9259118C');
        $this->addSql('ALTER TABLE item_order_item DROP FOREIGN KEY FK_EF5B3703E192A5F3');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE item_group');
        $this->addSql('DROP TABLE item_order');
        $this->addSql('DROP TABLE item_order_item');
    }
}
