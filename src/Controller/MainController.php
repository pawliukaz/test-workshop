<?php

namespace App\Controller;

use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main_index")
     * @param ItemRepository $itemRepository
     *
     * @return Response
     */
    public function index(ItemRepository $itemRepository): Response
    {
        return $this->render('index.html.twig', ['items' => $itemRepository->findAll()]);
    }
}
