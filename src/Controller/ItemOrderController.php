<?php

namespace App\Controller;

use App\Entity\ItemOrder;
use App\Form\ItemOrderType;
use App\Repository\ItemOrderRepository;
use App\Service\ItemService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/item-order")
 */
class ItemOrderController extends AbstractController
{
    /**
     * @Route("/", name="item_order_index", methods={"GET"})
     * @param ItemOrderRepository $itemOrderRepository
     *
     * @return Response
     */
    public function index(ItemOrderRepository $itemOrderRepository): Response
    {
        return $this->render(
            'item_order/index.html.twig',
            [
                'item_orders' => $itemOrderRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/new", name="item_order_new", methods={"GET","POST"})
     * @param Request     $request
     * @param ItemService $itemService
     *
     * @return Response
     */
    public function new(Request $request, ItemService $itemService): Response
    {
        $itemOrder = new ItemOrder();
        $form = $this->createForm(ItemOrderType::class, $itemOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $item = $itemService->createItemByOrder($itemOrder);
            $itemService->countItemQuantity($itemOrder, $item);
            $itemOrder->addItem($item);
            $entityManager->persist($itemOrder);
            $entityManager->flush();

            return $this->redirectToRoute('item_order_index');
        }

        return $this->render(
            'item_order/new.html.twig',
            [
                'item_order' => $itemOrder,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="item_order_show", methods={"GET"})
     * @param ItemOrder $itemOrder
     *
     * @return Response
     */
    public function show(ItemOrder $itemOrder): Response
    {
        return $this->render(
            'item_order/show.html.twig',
            [
                'item_order' => $itemOrder,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="item_order_edit", methods={"GET","POST"})
     * @param Request   $request
     * @param ItemOrder $itemOrder
     *
     * @return Response
     */
    public function edit(Request $request, ItemOrder $itemOrder): Response
    {
        $form = $this->createForm(ItemOrderType::class, $itemOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(
                'item_order_index',
                [
                    'id' => $itemOrder->getId(),
                ]
            );
        }

        return $this->render(
            'item_order/edit.html.twig',
            [
                'item_order' => $itemOrder,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="item_order_delete", methods={"DELETE"})
     * @param Request   $request
     * @param ItemOrder $itemOrder
     *
     * @return Response
     */
    public function delete(Request $request, ItemOrder $itemOrder): Response
    {
        if ($this->isCsrfTokenValid('delete'.$itemOrder->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($itemOrder);
            $entityManager->flush();
        }

        return $this->redirectToRoute('item_order_index');
    }
}
