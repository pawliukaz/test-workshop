<?php

namespace App\Controller;

use App\Entity\ItemGroup;
use App\Form\ItemGroupType;
use App\Repository\ItemGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/item-group")
 */
class ItemGroupController extends AbstractController
{
    /**
     * @Route("/", name="item_group_index", methods={"GET"})
     * @param ItemGroupRepository $itemGroupRepository
     *
     * @return Response
     */
    public function index(ItemGroupRepository $itemGroupRepository): Response
    {
        return $this->render(
            'item_group/index.html.twig',
            [
                'item_groups' => $itemGroupRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/new", name="item_group_new", methods={"GET","POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function new(Request $request): Response
    {
        $itemGroup = new ItemGroup();
        $form = $this->createForm(ItemGroupType::class, $itemGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($itemGroup);
            $entityManager->flush();

            return $this->redirectToRoute('item_group_index');
        }

        return $this->render(
            'item_group/new.html.twig',
            [
                'item_group' => $itemGroup,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="item_group_show", methods={"GET"})
     * @param ItemGroup $itemGroup
     *
     * @return Response
     */
    public function show(ItemGroup $itemGroup): Response
    {
        return $this->render(
            'item_group/show.html.twig',
            [
                'item_group' => $itemGroup,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="item_group_edit", methods={"GET","POST"})
     * @param Request   $request
     * @param ItemGroup $itemGroup
     *
     * @return Response
     */
    public function edit(Request $request, ItemGroup $itemGroup): Response
    {
        $form = $this->createForm(ItemGroupType::class, $itemGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(
                'item_group_index',
                [
                    'id' => $itemGroup->getId(),
                ]
            );
        }

        return $this->render(
            'item_group/edit.html.twig',
            [
                'item_group' => $itemGroup,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="item_group_delete", methods={"DELETE"})
     * @param Request   $request
     * @param ItemGroup $itemGroup
     *
     * @return Response
     */
    public function delete(Request $request, ItemGroup $itemGroup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$itemGroup->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($itemGroup);
            $entityManager->flush();
        }

        return $this->redirectToRoute('item_group_index');
    }
}
