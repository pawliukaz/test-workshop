<?php

namespace App\Service;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

abstract class BaseServiceAbstract
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ObjectRepository
     */
    protected $repository;

    /**
     * @var LoggerInterface|null
     */
    protected $logger;

    /**
     * BaseServiceAbstract constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param null|LoggerInterface   $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ?LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->repository = $this->getRepository();
    }

    /**
     * @param null|string $entryClass
     *
     * @return ObjectRepository
     */
    protected function getRepository($entryClass = null)
    {
        if ($entryClass != null) {
            return $this->entityManager->getRepository($entryClass);
        }

        return $this->entityManager->getRepository($this->getEntityClass());
    }

    /**
     * @param object $entity
     */
    protected function persist($entity)
    {
        $this->entityManager->persist($entity);
    }

    /**
     * void
     */
    protected function flush()
    {
        $this->entityManager->flush();
    }

    /**
     * @param null|object $objectName
     */
    protected function clear($objectName = null)
    {
        $this->entityManager->clear($objectName);
    }

    /**
     * @return string
     */
    abstract public function getEntityClass();
}
