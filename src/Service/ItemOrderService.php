<?php

namespace App\Service;

use App\Entity\ItemOrder;

class ItemOrderService extends BaseServiceAbstract
{
    /**
     * @var ItemOrderService
     */
    protected $repository;

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return ItemOrder::class;
    }
}
