<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\ItemOrder;
use App\Repository\ItemRepository;
use Doctrine\ORM\NonUniqueResultException;

class ItemService extends BaseServiceAbstract
{
    /**
     * @var ItemRepository
     */
    protected $repository;

    /**
     * @param ItemOrder $itemOrder
     * @param Item|null $item
     *
     * @return int
     */
    public function countItemQuantity(ItemOrder $itemOrder, ?Item $item): int
    {
        $quantity = $itemOrder->getQuantity();
        if ($item->getQuantity() != null) {
            $quantity = $item->getQuantity() + $itemOrder->getQuantity();

        }
        $item->setQuantity($quantity);
        $this->persist($item);

        return $quantity;
    }

    /**
     * @param ItemOrder $order
     *
     * @return Item|null
     */
    public function createItemByOrder(ItemOrder $order)
    {
        //TODO something missing here
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return Item::class;
    }
}
