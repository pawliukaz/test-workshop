<?php

namespace App\EventListener;

use App\Entity\TimestampableEntityInterface;
use App\Utils\DateTimeUtil;
use Doctrine\ORM\Event\LifecycleEventArgs;

class TimestampableListener
{
    /**
     * @var DateTimeUtil
     */
    protected $dateTimeUtil;

    /**
     * TimestampableListener constructor.
     *
     * @param DateTimeUtil $dateTimeUtil
     */
    public function __construct(DateTimeUtil $dateTimeUtil)
    {
        $this->dateTimeUtil = $dateTimeUtil;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TimestampableEntityInterface) {
            $entity->setModified($this->dateTimeUtil->getCurrentDate());
        }
    }
}
