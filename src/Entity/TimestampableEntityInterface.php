<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19/03/2019
 * Time: 23:42
 */

namespace App\Entity;


interface TimestampableEntityInterface
{
    /**
     * @return \DateTimeInterface
     */
    public function getCreated(): \DateTimeInterface;

    /**
     * @param \DateTimeInterface $created
     *
     * @return mixed
     */
    public function setCreated(\DateTimeInterface $created);

    /**
     * @return \DateTimeInterface|null
     */
    public function getModified(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface $modified
     *
     * @return mixed
     */
    public function setModified(\DateTimeInterface $modified);

}